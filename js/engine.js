$(function() {

const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();



$('.subscription-item__all').click(function() {
  $(this).toggleClass('active')
})



  
// Фиксированное меню при прокрутке страницы вниз

// $(window).scroll(function(){
//       var sticky = $('.header:not(.home .header)'),
//           scroll = $(window).scrollTop();
//       if (scroll > 200) {
//           sticky.addClass('header-fixed');
//       } else {
//           sticky.removeClass('header-fixed');
//       };
//   });


// FansyBox
 $('.fancybox').fancybox({});


// Аккордеон
$('.accordion-item__header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.accordion-item__body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.accordion-item__body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.accordion-item').hasClass('active'))){
    $('.accordion-item').removeClass("active");
    $(this).parent('.accordion-item').addClass("active");
  }else{
    $(this).parent('.accordion-item').removeClass("active");
  };
});


//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.tabs-mobile__btn').click(function() {
    var _targetElementParent = $(this).parent('.tabs-item');
    _targetElementParent.toggleClass('active-m');
    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;
});


// scrollbar
$('.scrollbar-inner').scrollbar({
  ignoreMobile: true
});




// Бургер меню
$(".burger-menu").click(function(){
  $(this).toggleClass("active");
  $('.header').toggleClass("active");
  $('.main-menu, .user-menu').fadeToggle(200);
});



// Вибор рынка
$('.select-market__btn').click(function() {
  $(this).toggleClass('active');
  $('.all-markets').fadeToggle();
})


// Видео слайдер
  $('.latest-videos__slider').slick({
  dots: false,
  arrows: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        adaptiveHeight: true
      }
    }
  ]
});



// Сообщения на мобильном
  $('.messages-list__toggler').click(function() {
    $(this).toggleClass('active')
    $(this).next('.messages-list').fadeToggle();
  })




})